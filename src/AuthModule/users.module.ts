import { TypeOrmModule } from "@nestjs/typeorm";
import { Module } from '@nestjs/common';

import { UsersRepository } from "./users.repository";
import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";
import { PassportModule } from "@nestjs/passport";
import { JwtModule } from "@nestjs/jwt";
import { JwtStrategy } from "./jwt.strategy";

@Module({
    // imports: [TypeOrmModule.forFeature([UsersRepository])],
    imports: [ PassportModule.register({ defaultStrategy: 'jwt' }), JwtModule.register({   secret: 'secret',   signOptions: {     expiresIn: 3600,   }, }), TypeOrmModule.forFeature([UsersRepository]),],
    controllers: [UsersController],
    providers: [UsersService,JwtStrategy],
    exports: [JwtStrategy,PassportModule,],

  })
  export class UsersModule {}



