import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UsePipes, ValidationPipe } from "@nestjs/common";
import { User } from "./user.entity";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/createUserdto";
import { PassportModule } from "@nestjs/passport";
import { JwtModule, JwtService } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UsersRepository } from "./users.repository";
import { JwtPayload } from "./jwt-payload.interface";
// import { UpdateUserDto } from "./dto/updateUserdto";
imports: [ PassportModule.register({ defaultStrategy: 'jwt' }), JwtModule.register({   secret: 'topSecret51',   signOptions: {     expiresIn: 3600,   }, }), TypeOrmModule.forFeature([UsersRepository]),]

@Controller('users')
export class UsersController {
          constructor(
            private  usersService: UsersService,
            private jwtService: JwtService,) {}

    @Get()
    async getAllUsers(): Promise<User[]> {
      return this.usersService.getAllUsers();
    }

    @Get('/:id')
    async getTaskById(@Param('id', ParseIntPipe) id: number): Promise<User> {
      return this.usersService.getUserById(id);
    }
    @Post('/createUser')
    @UsePipes(ValidationPipe)
    createUser(@Body() createUserDto: CreateUserDto) {
      console.log("Create User");
      return this.usersService.createUser(createUserDto);
    }

    @Delete('/:id')
    deleteTaskById(@Param('id') id: number):Promise<number>{
      return this.usersService.deleteUserById(id);
  }
//   @Patch('/updateUser')
//   @UsePipes(ValidationPipe)
//   updateUser(@Body() updateUserDTO: UpdateUserDto):Promise<User> {
//     return this.usersService.updateUser(updateUserDTO);
//   }
 
@Post('/signin')
async signin(@Body() createUserDto: CreateUserDto) {
  // call service to connect

  const user = await this.usersService.signin(createUserDto);
  const payload: JwtPayload = {
    mail: user.mail,
    password: user.password,

  };
  const accessToken = await this.jwtService.sign(payload);
  return { accessToken };
}



}
